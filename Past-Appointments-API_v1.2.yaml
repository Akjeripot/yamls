swagger: "2.0"
info:
  version: 1.0.0
  title: Past Appointments API
  description: The EPIC Past Appointments Details Consumer API RESTful web service is a secure service requiring an authentication token. This service API calls EPIC Patient Access Mobile (SOAP) service and transforms the response from SOAP to JSON. The service supports ability to get details of member’s single visit past appointment by furnishing the appointment ID , including the Provider Dr. Photo URL. This API will return the single visit past appointment details for all the regions including NCAL.
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : ""
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
produces:
  - application/json
basePath: /
paths:
  /mycare/v1.2/pastappointmentdetails:
    get:
      description: |
            Returns the single visit past appointment details for all the regions including NCAL.
      operationId: getpastappointmentdetails
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: X-apiKeyCPM
          in: header
          description: Valid API Key. API Key specific to the consumer
          required: true
          type: string
        - name: X-sessionToken
          in: header
          description: "true or false, Browser based consumer who don’t use Signon API to get authenticated should send this header as true so that Axway will return SessionToken header back in any API with get HTTP verb. Consumer should send SessionToken in further API that uses PUT/POST/DELETE operation/verbs otherwise global API will return http status code 403."
          default: true
          required: false
          type: boolean
        - name: X-resourceInd
          in: header
          description: Resource indicator for example cpm / npi / localId / license / upin / tin / dea / hms 
          default: 'cpm'
          required: true
          type: string
        - name: X-resourceId
          in: header
          description: Id value of resource indicator 
          required: true
          type: string
        - name: X-state
          in: header
          description: Mandatory for localId and optional for license 
          required: false
          type: string
        - name: id
          in: query
          description: Service uses query parameter ( ?id = 12345 ) to return details of the single visit appointment having this appointment ContactId.
          default: '57798'
          required: true
          type: string 

      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema: 
            $ref: "#/definitions/GetPastAppointmentDetailsResult"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'

definitions:
    GetPastAppointmentDetailsResult:
          type: object
          properties:
            GetPastAppointmentDetailsResult:
              type: object
              properties:
                ContactID:
                  type: 'null'
                Date:
                  type: string
                IsSurgery:
                  type: boolean
                IsTimeNull:
                  type: boolean
                PatientInstruction:
                  type: 'null'
                Providers:
                  type: array
                  items:
                    type: object
                    properties:
                      ResourceId:
                        type: string
                      DoctorURL:
                        type: string
                    required:
                    - ResourceId
                    - DoctorURL
                VisitType:
                  type: string
                IsClinicalInformationAvailable:
                  type: boolean
                IsEDVisit:
                  type: boolean
                Avs_order:
                  type: 'null'
                Diagnoses:
                  type: 'null'
                FollowUpInstructions:
                  type: 'null'
                Medications:
                  type: array
                  items: {}
                Orders:
                  type: array
                  items: {}
                Reasons:
                  type: 'null'
                Vitals:
                  type: object
                  properties:
                    BloodPressure:
                      type: 'null'
                    Height:
                      type: 'null'
                    Pulse:
                      type: 'null'
                    Resp:
                      type: 'null'
                    Temperature:
                      type: 'null'
                    TemperatureSource:
                      type: 'null'
                    Weight:
                      type: 'null'
                  required:
                  - BloodPressure
                  - Height
                  - Pulse
                  - Resp
                  - Temperature
                  - TemperatureSource
                  - Weight
              required:
              - ContactID
              - Date
              - IsSurgery
              - IsTimeNull
              - PatientInstruction
              - Providers
              - VisitType
              - IsClinicalInformationAvailable
              - IsEDVisit
              - Avs_order
              - Diagnoses
              - FollowUpInstructions
              - Medications
              - Orders
              - Reasons
              - Vitals
          required:
          - GetPastAppointmentDetailsResult

    503ErrorRespSchema:
        type: object
        properties:
          executionContext:
            type: object
            properties:
              errors:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              warnings:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              infos:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
            required:
            - errors
            - warnings
            - infos
        required:
        - executionContext
        
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: 'Device OS Version. For example 5.1.' 
    default: '5.1'
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string

responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  405Error:
      description: "Method not supported"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"