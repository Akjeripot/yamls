swagger: '2.0'
info:
  version: "2.0"
  title: User API
  description : "This API returns all the data for a User that is known to the UserDataService garnered from MemberWebServices getMembershipDemographicData4EMAFM request, the User Data Repository and the LDAP instances."
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : "https://sites.sp.kp.org/projects/csdt/APIIS/integrationServices/Shared%20Documents/APIs/Sign%20On%20Custom%20API%20V2.1.docx"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
consumes:
  - application/json
produces:
  - application/json
#host: api-hint10.kaiserpermanente.org
# Describe your paths here
paths:
  /care/v2.0/token:
    get:
      description:
        API to get the Security Token for the user
      produces:
       - application/json
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: Authorization
          in: header
          description: "HTTP Basic Authentication.  UserId & Password must be base64 encoded."
          required: true
          type: string
        - name: X-sessionToken
          in: header
          description: "If it is set to true, LtpaToken2 cookie is required"
          required: false
          type: string
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/UserInfo"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'  
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
definitions:
    UserInfo:
      type: object
      properties:
        systemError:
          type: 'null'
          default: 
        interruptList:
          type: array
          items: {}
        success:
          type: boolean
          default: true
        businessError:
          type: 'null'
          default: 
        user:
          type: object
          properties:
            region:
              type: string
              default: SCA
            lastName:
              type: string
              default: HCOTWPPSCLFGDIICCJHJDLN
            termsAndCondAccepted:
              type: string
              default: 1.6#1439843288665
            activationStatusCode:
              type: string
              default: ACTIVE
            preferredFirstName:
              type: string
              default: WPPSCLFJCDGFIEHGJFN
            memberShipAccountEnrollmentUnit:
              type: string
              default: '0'
            memberShipPlanPurchaseId:
              type: string
              default: '231394'
            guid:
              type: string
              default: '10002926'
            mobilePhone:
              type: string
              default: '5115655454'
            email:
              type: string
              default: test@kp.org
            ebizAccountRoles:
              type: array
              items:
                type: string
                default: MBR
            disabledReasonCode:
              type: 'null'
              default: 
            firstName:
              type: string
              default: WPPSCLFJCDGFIEHGJFN
            epicEmail:
              type: string
              default: test@kp.org
            serviceArea:
              type: 'null'
              default: 
            age:
              type: integer
              default: 35
          required:
          - region
          - lastName
          - termsAndCondAccepted
          - activationStatusCode
          - preferredFirstName
          - memberShipAccountEnrollmentUnit
          - memberShipPlanPurchaseId
          - guid
          - mobilePhone
          - email
          - ebizAccountRoles
          - disabledReasonCode
          - firstName
          - epicEmail
          - serviceArea
          - age
        failureInfo:
          type: 'null'
          default: 
      required:
      - systemError
      - interruptList
      - success
      - businessError
      - user
      - failureInfo
    
    412ErrorRespSchema:      
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors      
    503ErrorRespSchema:
        type: object
        properties:
          errors:
            type: array
            items:
              type: object
              properties:
                desc:
                  type: string
                type:
                  type: string
              required:
              - desc
              - type
        required:
        - errors
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: Device OS Version. For example 5.1. 
    default: 5.1.
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  405Error:
          description: "HTTP method is not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            X-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
            Allow:
              type: string
              description : "Allowed HTTP Methods"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      schema:
          $ref : "#/definitions/412ErrorRespSchema"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"

