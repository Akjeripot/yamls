swagger: "2.0"
info:
  version: 1.0.0
  title: Appointment Provider API
  description: The EPIC Appointments Consumer API RESTful web service is a secure service requiring an authentication token. This service API calls EPIC Patient Access Mobile (SOAP) service and transforms the response from SOAP to JSON. The service supports ability to get member’s up-coming appointments, to schedule an appointment and to cancel an appointment.
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : "https://sites.sp.kp.org/projects/csdt/APIIS/integrationServices/_layouts/15/WopiFrame.aspx?sourcedoc=/projects/csdt/APIIS/integrationServices/Shared%20Documents/APIs/EPIC_Appointments_API_Document.doc&action=default"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
consumes:
  - application/json
produces:
  - application/json
paths:
  /mycare/v1.0/appointment/providers:
    get:
      description: |
          Get scheduling providers’ information for logged in user or proxy subject.      
      operationId: getProviders
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: X-rsnIndex
          in: header
          description: "Received as Index in ReasonsForVisit array in Scenario 2 - Step 1."
          required: true
          type: string
        
        - name: relid
          in: query
          description: "Proxy relationid"
          required: false
          type: string
     
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/providersResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
definitions:
  providersResponse:
      type: object
      properties:
        SchedulingProvidersInfo:
          type: object
          properties:
            CanSchedule:
              type: boolean
              default: true
            Centers:
              type: array
              items:
                type: object
                properties:
                  Name:
                    type: string
                    default: " SKYLINE MEDICAL OFFICE"
                  Value:
                    type: integer
                    default: 200612
                required:
                - Name
                - Value
            Error:
              type: 'null'
              default: 
            SchedulingDepartments:
              type: array
              items:
                type: object
                properties:
                  Address:
                    type: object
                    properties:
                      City:
                        type: string
                        default: PORTLAND
                      Country:
                        type: 'null'
                        default: 
                      County:
                        type: 'null'
                        default: 
                      District:
                        type: 'null'
                        default: 
                      HouseNumber:
                        type: 'null'
                        default: 
                      Lines:
                        type: array
                        items:
                          type: string
                          default: 2850 N W NICOLAI
                      State:
                        type: object
                        properties:
                          Name:
                            type: string
                            default: Oregon
                          Value:
                            type: integer
                            default: 38
                        required:
                        - Name
                        - Value
                      ZIP:
                        type: integer
                        default: 97210
                    required:
                    - City
                    - Country
                    - County
                    - District
                    - HouseNumber
                    - Lines
                    - State
                    - ZIP
                  Center:
                    type: object
                    properties:
                      Name:
                        type: 'null'
                        default: 
                      Value:
                        type: integer
                        default: 200597
                    required:
                    - Name
                    - Value
                  ID:
                    type: integer
                    default: 396
                  Name:
                    type: string
                    default: Nicolai Family Practice
                  Phone:
                    type: string
                    default: 800-813-2000
                  Specialty:
                    type: object
                    properties:
                      Name:
                        type: string
                        default: Family Practice
                      Value:
                        type: integer
                        default: 18
                    required:
                    - Name
                    - Value
                required:
                - Address
                - Center
                - ID
                - Name
                - Phone
                - Specialty
            SchedulingProviders:
              type: array
              items:
                type: object
                properties:
                  DepartmentIDs:
                    type: array
                    items:
                      type: integer
                      default: 396
                  ID:
                    type: string
                    default: " 205647"
                  IsPCP:
                    type: boolean
                    default: false
                  Languages:
                    type: 'null'
                    default: 
                  Name:
                    type: string
                    default: George, Alan
                  PhotoURL:
                    type: 'null'
                    default: 
                  Sex:
                    type: object
                    properties:
                      Name:
                        type: string
                        default: Male
                      Value:
                        type: integer
                        default: 2
                    required:
                    - Name
                    - Value
                  Specialties:
                    type: array
                    items:
                      type: object
                      properties:
                        Name:
                          type: string
                          default: General Practice
                        Value:
                          type: integer
                          default: 311
                      required:
                      - Name
                      - Value
                required:
                - DepartmentIDs
                - ID
                - IsPCP
                - Languages
                - Name
                - PhotoURL
                - Sex
                - Specialties
          required:
          - CanSchedule
          - Centers
          - Error
          - SchedulingDepartments
          - SchedulingProviders
      required:
      - SchedulingProvidersInfo

  412ErrorRespSchema:      
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors      
  503ErrorRespSchema:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: Device OS Version. For example 5.1. 
    default: 5.1.
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
    
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      schema:
          $ref : "#/definitions/412ErrorRespSchema"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"
