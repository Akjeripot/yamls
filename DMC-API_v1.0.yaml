swagger: '2.0'
info:
  version: "1.0"
  title: DMC API
  description : "Digital Membership Card Service RESTful API Specification"
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "`API Wiki Link`"
  url : "http://wiki-tech.kp.org/mediawiki/index.php/DMC_API_v1.0"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
consumes:
  - application/json
produces:
  - application/json
# Describe your paths here
paths:
  /mycare/dmc/v1.0/memcard:
    get:
      description: |
        Returns membership CardData.
      produces:
        - application/json
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: X-RELATIONSHIP-ID
          in: header
          description: Specified only when the logged in user has proxy subjects associated
          required: false
          type: string
        - name: X-CorrelationID
          in: header
          description: used to correlate the synchronous request and response messages if necessary.
          required: false
          type: string
        - name: Accept
          in: header
          description: application/json
          required: false
          type: string
        - name: freshness
          in: query
          description: Value in minutes
          required: false
          type: integer
        - name: cmsfilter
          in: query
          description: enable = true – the default or disable = false
          required: false
          type: boolean
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/membershipCardData"
        206:
          description: "Partial response => Not all MRNs could fetched; one of the data sources is unavailable"
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID corresponding to the failed MRN/s"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/membershipCardData"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'  
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
definitions:
  membershipCardData:
    type: object
    properties:
      membershipCardData:
        type: array
        items:
          type: object
          properties:
            memberDemographics:
              type: object
              properties:
                memberName:
                  type: object
                  properties:
                    firstName:
                      type: string
                      default: Coronus
                    lastName:
                      type: string
                      default: Alexander
                    fullName:
                      type: string
                      default: Coronus Alexander
                  required:
                  - firstName
                  - lastName
                  - fullName
                gender:
                  type: string
                  default: M
                birthDate:
                  type: string
                  default: '1950-01-31'
                regionCode:
                  type: string
                  default: SCA
                regionName:
                  type: string
                  default: Southern California Region
              required:
              - memberName
              - gender
              - birthDate
              - regionCode
              - regionName
            membershipAccountDetails:
              type: object
              properties:
                medicalRecordNumber:
                  type: string
                  default: '11235813'
                medicalRecordNumberPrefix:
                  type: string
                  default: '00'
                membershipAccountStatus:
                  type: string
                  default: ACTIVE
                relationshipID:
                  type: string
                  default: '31853211'
              required:
              - medicalRecordNumber
              - medicalRecordNumberPrefix
              - membershipAccountStatus
              - relationshipID
            membershipPlanInfo:
              type: object
              properties:
                coverageStartDate:
                  type: string
                  default: '2014-01-01'
                coverageEndDate:
                  type: string
                  default: '2014-12-31'
                groupId:
                  type: string
                  default: '226977'
                dmcConsumerPlanType:
                  type: string
                  default: KFHP_HMO
                dmcId:
                  type: string
                  default: KFHP_HMO:226977
                selfFunded:
                  type: string
                  default: 'false'
              required:
              - coverageStartDate
              - coverageEndDate
              - groupId
              - dmcConsumerPlanType
              - dmcId
              - selfFunded
            healthPlanDescriptors:
              type: object
              properties:
                kaiserEntityName:
                  type: string
                  default: KFHP
              required:
              - kaiserEntityName
            serviceContactDetails:
              type: object
              properties:
                healthPlanBenefitsContactNumber:
                  type: object
                  properties:
                    inNetwork:
                      type: object
                      properties:
                        voice:
                          type: object
                          properties:
                            tollFreeNumber:
                              type: string
                              default: 1-800-464-4000
                          required:
                          - tollFreeNumber
                        tty:
                          type: object
                          properties:
                            tollFreeNumber:
                              type: string
                              default: 1-800-777-1370
                          required:
                          - tollFreeNumber
                      required:
                      - voice
                      - tty
                  required:
                  - inNetwork
                afterHoursNurseAdviceContactNumber:
                  type: object
                  properties:
                    inNetwork:
                      type: object
                      properties:
                        voice:
                          type: object
                          properties:
                            tollFreeNumber:
                              type: string
                              default: 1-888-576-6225
                          required:
                          - tollFreeNumber
                        tty:
                          type: object
                          properties:
                            tollFreeNumber:
                              type: string
                              default: 1-888-880-0833
                          required:
                          - tollFreeNumber
                      required:
                      - voice
                      - tty
                  required:
                  - inNetwork
                emergencyCareInNonPlanHospitalContactNumber:
                  type: object
                  properties:
                    inNetwork:
                      type: object
                      properties:
                        voice:
                          type: object
                          properties:
                            tollFreeNumber:
                              type: string
                              default: 1-800-225-8883
                          required:
                          - tollFreeNumber
                        tty:
                          type: object
                          properties:
                            tollFreeNumber:
                              type: string
                              default: 1-800-777-1370
                          required:
                          - tollFreeNumber
                      required:
                      - voice
                      - tty
                  required:
                  - inNetwork
              required:
              - healthPlanBenefitsContactNumber
              - afterHoursNurseAdviceContactNumber
              - emergencyCareInNonPlanHospitalContactNumber
            logosAndURLs:
              type: object
              properties:
                kpWebsiteUrl:
                  type: string
                  default: kp.org
                kpLogoUrl_Root:
                  type: string
                  default: kpLogoUrl_Root0
                kpLogoUrl_Image:
                  type: string
                  default: kp.png
                otherComplainceLogoUrl1_Root:
                  type: string
                  default: otherComplainceLogoUrl1_Root0
                otherComplainceLogoUrl1_Image:
                  type: string
                  default: lacare.png
              required:
              - kpWebsiteUrl
              - kpLogoUrl_Root
              - kpLogoUrl_Image
              - otherComplainceLogoUrl1_Root
              - otherComplainceLogoUrl1_Image
            legalDisclaimers:
              type: object
              properties:
                emergencyCareDisclaimer:
                  type: string
                  default: If you receive emergency care in a non-KP hospital, call 1-800-225-8883/TTY
                    1-800-777-1370 as soon as your condition is stabilized so that a Kaiser
                    Permanente physician can access your medical information to discuss
                    your care with the treating physician. Your call to obtain authorization
                    for post-stabilization care may also help protect you from financial
                    responsibility.
                disclaimer911:
                  type: string
                  default: If you think you have a medical or psychiatric emergency call
                    911 or go to the nearest hospital.
                idCardIdentificationOnlyDisclaimer:
                  type: string
                  default: This card is for identification only. Possession of this card
                    confers no right to services or benefits unless the holder is a member
                    complying with all provisions of an applicable agreement.
                entityRelationshipDisclaimer:
                  type: string
                  default: Pharmacy Benefit Underwritten by KPIC
              required:
              - emergencyCareDisclaimer
              - disclaimer911
              - idCardIdentificationOnlyDisclaimer
              - entityRelationshipDisclaimer
          required:
          - memberDemographics
          - membershipAccountDetails
          - membershipPlanInfo
          - healthPlanDescriptors
          - serviceContactDetails
          - logosAndURLs
          - legalDisclaimers
    required:
    - membershipCardData

  412ErrorRespSchema:      
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors      
  503ErrorRespSchema:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: Device OS Version. For example 5.1. 
    default: 5.1.
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  405Error:
          description: "HTTP method is not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            X-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
            Allow:
              type: string
              description : "Allowed HTTP Methods"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      schema:
          $ref : "#/definitions/412ErrorRespSchema"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"
