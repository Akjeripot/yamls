swagger: '2.0'
info:
  version: "1.0"
  title: Audit API
  description : "The Audit API for User Audit"
  contact:
    name: APISERVICES_TEAM
    email: APISERVICES_TEAM@domino.kp.org
schemes:
        - https
externalDocs:
  description: "API Wiki Link"
  url : "http://wiki-tech.kp.org/mediawiki/index.php/AxwayAPIs/Audit_API_1.0"
host: xjzxqws011fx.dta.kp.org:8080
# will be prefixed to all paths
basePath: /
# Describe your paths here
paths:
  /mycare/v1.0/audit:
    post:
      description:
        Audit operation
      consumes:
        - application/json
      produces:
       - application/json
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name : userId
          in: body
          description: "userId parameter"
          required : false
          schema:
            $ref : "#/definitions/AuditRequest"
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              description: "ssosession header received from sign-on service."
              type: string
          schema:
            $ref : "#/definitions/AuditResponse"
        404:
          description: "Resource Not Found"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        405:
          description: "HTTP method is not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description: "Application name i.e. kp-mobile_v2.0, KP Mobile"
            X-osversion:
              type: string
              description: "Version of the device OS i.e. 5.1"
            X-useragentcategory:
              type: string
              description: "I = iOS, A = Android, M = mobile web"
            X-useragenttype:
              type: string
              description: "Type of agent. For example iPhone5, Samsung etc."
            ssosession:
              description: "ssosession header received from sign-on service."
              type: string
            Allow:
              type: string
              description : "Allowed HTTP Methods"
        412:
          description: "Precondition Failed"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description: "Application name i.e. kp-mobile_v2.0, KP Mobile"
            X-osversion:
              type: string
              description: "Version of the device OS i.e. 5.1"
            X-useragentcategory:
              type: string
              description: "I = iOS, A = Android, M = mobile web"
            X-useragenttype:
              type: string
              description: "Type of agent. For example iPhone5, Samsung etc."
            ssosession:
              description: "ssosession header received from sign-on service."
              type: string
        503:
          description: "Referenced Backend Service is not available/System Error"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description: "Application name i.e. kp-mobile_v2.0, KP Mobile"
            X-osversion:
              type: string
              description: "Version of the device OS i.e. 5.1"
            X-useragentcategory:
              type: string
              description: "I = iOS, A = Android, M = mobile web"
            X-useragenttype:
              type: string
              description: "Type of agent. For example iPhone5, Samsung etc."
            ssosession:
              description: "ssosession header received from sign-on service."
              type: string
          schema:
               $ref : "#/definitions/ErrorSchema"
        500:
          description: "Internal Error in Axway"
definitions:
  AuditRequest:
      type: object
      properties:
        userId:
          type: string
          default: userid20497834
        actorUserId:
          type: string
          default: "--KP.org SupportNet USERID--"
        actorUuid:
          type: string
          default: "--KP.org SupportNet GUID--"
        featureName:
          type: string
          default: Video Visit
        portletName:
          type: string
          default: View Appointments
        txTypeCode:
          type: string
          default: START_VIDEO_VISIT
        txResultCode:
          type: string
          default: S
        txReason:
          type: string
          default: "--Any failure reason here--"
        basicAuditDetails:
          type: string
          default: "--Patient has started the launch sequence for a video visit.--"
        sensitiveAuditDetails:
          type: string
          default: "--Patient PHI info such as MRN etc. here--"
      required:
      - userId
      - actorUserId
      - actorUuid
      - featureName
      - portletName
      - txTypeCode
      - txResultCode
      - txReason
      - basicAuditDetails
      - sensitiveAuditDetails

  AuditResponse:
      type: object
      properties:
        statusType:
          type: string
          default: OK
        entity:
          type: string
          default: Audit record created successfully!
        entityType:
          type: string
          default: java.lang.String
        status:
          type: integer
          default: 200
        metadata:
          type: object
          properties:
            Content-Type:
              type: array
              items:
                type: object
                properties:
                  type:
                    type: string
                    default: application
                  subtype:
                    type: string
                    default: json
                  parameters:
                    type: object
                    properties: {}
                  wildcardType:
                    type: boolean
                    default: false
                  wildcardSubtype:
                    type: boolean
                    default: false
                required:
                - type
                - subtype
                - parameters
                - wildcardType
                - wildcardSubtype
          required:
          - Content-Type
      required:
      - statusType
      - entity
      - entityType
      - status
      - metadata

  ErrorSchema:
    type: object
    properties:
      executionContext:
        type: object
        properties:
          errors:
            type: array
            items:
              type: object
              properties:
                system:
                  type: string
                message:
                  type: string
                code:
                  type: integer
              required:
              - system
              - message
              - code
          warnings:
            type: array
            items:
              type: object
              properties:
                system:
                  type: string
                message:
                  type: string
                code:
                  type: integer
              required:
              - system
              - message
              - code
          infos:
            type: array
            items:
              type: object
              properties:
                system:
                  type: string
                message:
                  type: string
                code:
                  type: integer
              required:
              - system
              - message
              - code
        required:
        - errors
        - warnings
        - infos
    required:
    - executionContext
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: 'Device OS Version. For example 5.1.' 
    default: '5.1'
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
