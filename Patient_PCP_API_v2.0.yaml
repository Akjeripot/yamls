swagger: "2.0"
info:
  version: 2.0.0
  title: Patient PCP API
  description: PatientPCP API returns Patient care team or primary care physicians for logged in user or for any proxy of the logged in user.
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : "http://wiki-tech.kp.org/mediawiki/index.php/AxwayAPIs/Patient_PCP_API_v2.0"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
consumes:
  - application/json
produces:
  - application/json
paths:
  /mycare/v2.0/patientpcp:
    get:
      description: |
          The API returns Patient care team or primary care physicians for logged in user or for any proxy of the logged in user.      
      operationId: getPCP
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: X-sessionToken
          in: header
          description: true/false, Browser based consumer who don’t use Signon API to get authenticated should send this header as true so that Axway will return SessionToken header back in any API with get HTTP verb. Consumer should send SessionToken in further API that uses PUT/POST/DELETE operation/verbs otherwise global API will return http status code 403.
          required: false
          type: boolean
        - name: X-relId
          in: header
          description: "Epic Relationship ID. Required only when requesting Epic messages on behalf of a subject."
          default: '123'
          required: false
          type: string
        - name: X-resourceType
          in: header
          description: "Filters providers based on matched CPM resourceType. Default is blank and returns all providers."
          required: false
          type: string  
        
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/patientPCPResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
definitions:
  patientPCPResponse:
      type: object
      properties:
        patient:
          type: array
          items:
            type: object
            properties:
              home:
                type: string
                example: urn:kp:hcotca2
              sex:
                type: string
                example: Male
              patientName:
                type: string
                example: WPPMRNIEFHBGGDFDILN,WPPMRNCDFHGEDJHFBFN
              patientPCP:
                type: array
                items: {}
              relId:
                type: string
                example: 611GWOWXQCT
              patientIDType:
                type: string
                example: NCALMRN
            required:
            - home
            - sex
            - patientName
            - patientPCP
            - relId
            - patientIDType
      required:
      - patient

  412ErrorRespSchema:      
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors      
  503ErrorRespSchema:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: 'Device OS Version. For example 5.1.' 
    default: '5.1'
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string

responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      schema:
          $ref : "#/definitions/412ErrorRespSchema"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"
