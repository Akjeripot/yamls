swagger: '2.0'
info:
  version: "1.2"
  title: Patient Demographics API
  description : "The DemographicsService Consumer API RESTful web service is a secure service requiring an authentication token. These service APIs convert the incoming JSON objects to SOAP messages, call the DemographicsService (SOAP) service and transform the DemographicsService response from SOAP to JSON."
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
schemes:
        - https
externalDocs:
  description: "`API Wiki Link`"
  url : "http://wiki-tech.kp.org/mediawiki/index.php/AxwayAPIs/PatientDemographics_API_1.2"
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
# Describe your paths here
paths:  
  /mycare/appointments/v1.2/patientdemographics:
    get:
      description: |
        Returns a patient demographic data for the logged in user or the proxy subject selected
      produces:
        - application/json
      tags:
        - GET
      parameters:
        - name: X-apiKey
          in: header
          description: VAPI Key specific to the consumer, for e.g. kaisermobil93908784817875726966
          default: kprwd65766367497853935616
          required: true
          type: string
        - name: X-appName
          in: header
          description: Application name i.e. kp‐mobile_v2.0, KP Mobile
          default: RWD
          required: true
          type: string
        - name: X-useragenttype
          in: header
          description: Type of agent. For example iPhone5, Samsung etc.
          default: iphone4
          required: true
          type: string
        - name: X-useragentcategory
          in: header
          description: I = iOS, A = Android, M = mobile web
          default: I
          required: true
          type: string
        - name: X-osversion
          in: header
          description: Version of the device OS i.e. 5.1
          default: '5.1'
          required: true
          type: string
        - name: ssosession
          in: header
          description: ssosession header received from sign-on service. Required for authorization, Either ssoseesion header or ObSSOCookie is required
          default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
          required: true
          type: string
        - name: ObSSOCookie
          in: header
          description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
          required: false
          type: string
        - name: relid
          in: query
          description: Proxy relationid
          default: '613HIWDXOXO'
          required: false
          type: string
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            Cookie:
              type: string
              description: "Cookie data from request"
          schema:
            $ref : "#/definitions/GetPatientDemographicsResponse"
        400:
          description: "Bad Request."
        401:
          description: "Unauthorized."
        403:
          description: "Access Denied. User is not entitled to access the requested API."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        404:
          description: "Resource not found."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        405:
          description: "Method not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            X-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "Cookie Data"
            Allow:
              type: string
              description : "Allowed HTTP Methods"
        412:
          description: "PreCondition failed."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        500:
          description: "Internal Error in Axway"
        503:
          description: "Service Unavailable."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
          schema:
                $ref : "#/definitions/503ErrorRespSchema"              
  
    put:
      description: |
        Accepts patient demographic data for the logged in user or the proxy subject selected and Returns the method used to update, reason and errors
      produces:
        - application/json
      tags:
        - PUT
      parameters:
        - name: X-apiKey
          in: header
          description: VAPI Key specific to the consumer, for e.g. kaisermobil93908784817875726966
          default: kprwd65766367497853935616
          required: true
          type: string
        - name: X-appName
          in: header
          description: Application name i.e. kp‐mobile_v2.0, KP Mobile
          default: RWD
          required: true
          type: string
        - name: X-useragenttype
          in: header
          description: Type of agent. For example iPhone5, Samsung etc.
          default: iphone4
          required: true
          type: string
        - name: X-useragentcategory
          in: header
          description: I = iOS, A = Android, M = mobile web
          default: I
          required: true
          type: string
        - name: X-osversion
          in: header
          description: Version of the device OS i.e. 5.1
          default: '5.1'
          required: true
          type: string
        - name: ssosession
          in: header
          description: ssosession header received from sign-on service. Required for authorization, Either ssoseesion header or ObSSOCookie is required
          default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
          required: true
          type: string
        - name: ObSSOCookie
          in: header
          description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
          required: false
          type: string
        - name: relid
          in: query
          description: Proxy relationid
          default: '613HIWDXOXO'
          required: false
          type: string
        - name: body
          in: body
          description: Required Details for Demographics
          required: true
          schema:
            $ref : "#/definitions/request"
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            Cookie:
              type: string
              description: "Cookie data from request"
          schema:
            $ref : "#/definitions/UpdatePatientDemographicsResponse"
        400:
          description: "Bad Request."
        401:
          description: "Unauthorized."
        403:
          description: "Access Denied. User is not entitled to access the requested API."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        404:
          description: "Resource not found."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        405:
          description: "Method not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            X-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "Cookie Data"
            Allow:
              type: string
              description : "Allowed HTTP Methods"
        412:
          description: "PreCondition failed."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        500:
          description: "Internal Error in Axway"
        503:
          description: "Service Unavailable."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
          schema:
                $ref : "#/definitions/503ErrorRespSchema"              
              
definitions:
  request:
      type: object
      properties:
        Address:
          type: object
          properties:
            City:
              type: string
              default: Pleasanton
            Country:
              type: string
              default: UNITED STATES
            County:
              type: string
              default: WASHINGTON
            District:
              type: 'null'
              default: 
            HouseNumber:
              type: string
              default: 123/abc-1
            PostalCode:
              type: string
              default: '20057'
            State:
              type: string
              default: DC
            StreetAddress:
              type: array
              items:
                type: object
                properties:
                  string:
                    type: string
                    default: address3
                required:
                - string
          required:
          - City
          - Country
          - County
          - District
          - HouseNumber
          - PostalCode
          - State
          - StreetAddress
        Emails:
          type: array
          items:
            type: object
            properties:
              string:
                type: string
                default: goutham04@gmail.com
            required:
            - string
        EthnicBackground:
          type: array
          items: {}
        EthnicGroup:
          type: string
          default: Alsatian
        Language:
          type: string
          default: English
        Race:
          type: array
          items: {}
        Phone:
          type: array
          items:
            type: object
            properties:
              Type:
                type: string
                default: W
              Number:
                type: string
                default: 231-679-4566 x91939327937
            required:
            - Type
            - Number
        UpdateRaceEthLang:
          type: boolean
          default: true
      required:
      - Address
      - Emails
      - EthnicBackground
      - EthnicGroup
      - Language
      - Race
      - Phone
      - UpdateRaceEthLang

  GetPatientDemographicsResponse:
    type: object
    properties:
      GetPatientDemographicsResult:
        type: object
        properties:
          Demographics:
            type: object
            properties:
              Address:
                type: object
                properties:
                  City:
                    type: string
                  Country:
                    type: object
                    properties:
                      Abbreviation:
                        type: string
                      Number:
                        type: integer
                      Title:
                        type: string
                    required:
                    - Abbreviation
                    - Number
                    - Title
                  County:
                    type: object
                    properties:
                      Abbreviation:
                        type: string
                      Number:
                        type: integer
                      Title:
                        type: string
                    required:
                    - Abbreviation
                    - Number
                    - Title
                  District:
                    type: object
                    properties:
                      Abbreviation:
                        type: string
                      Number:
                        type: string
                      Title:
                        type: string
                    required:
                    - Abbreviation
                    - Number
                    - Title
                  HouseNumber:
                    type: string
                  PostalCode:
                    type: integer
                  State:
                    type: object
                    properties:
                      Abbreviation:
                        type: string
                      Number:
                        type: integer
                      Title:
                        type: string
                    required:
                    - Abbreviation
                    - Number
                    - Title
                  StreetAddress:
                    type: array
                    items:
                      type: string
                required:
                - City
                - Country
                - County
                - District
                - HouseNumber
                - PostalCode
                - State
                - StreetAddress
              Emails:
                type: array
                items:
                  type: string
              EthnicBackground:
                type: array
                items: {}
              EthnicGroup:
                type: object
                properties:
                  Abbreviation:
                    type: string
                  Number:
                    type: integer
                  Title:
                    type: string
                required:
                - Abbreviation
                - Number
                - Title
              Language:
                type: object
                properties:
                  Abbreviation:
                    type: string
                  Number:
                    type: string
                  Title:
                    type: string
                required:
                - Abbreviation
                - Number
                - Title
              Phones:
                type: array
                items:
                  type: object
                  properties:
                    ContactNumber:
                      type: string
                    NumberType:
                      type: object
                      properties:
                        Abbreviation:
                          type: string
                        Number:
                          type: integer
                        Title:
                          type: string
                      required:
                      - Abbreviation
                      - Number
                      - Title
                  required:
                  - ContactNumber
                  - NumberType
              Race:
                type: array
                items: {}
            required:
            - Address
            - Emails
            - EthnicBackground
            - EthnicGroup
            - Language
            - Phones
            - Race
          DemographicsUpdateNeeded:
            type: boolean
          Errors:
            type: array
            items:
              type: object
              properties:
                Code:
                  type: string
                Description:
                  type: string
              required:
              - Code
              - Description
          RaceEthLangUpdateNeeded:
            type: boolean
        required:
        - Demographics
        - DemographicsUpdateNeeded
        - Errors
        - RaceEthLangUpdateNeeded
    required:
    - GetPatientDemographicsResult
    
  UpdatePatientDemographicsResponse:
    type: object
    properties:
      UpdatePatientDemographicsResult:
        type: object
        properties:
          Errors:
            type: array
            items:
              type: object
              properties:
                Code:
                  type: string
                Description:
                  type: string
              required:
              - Code
              - Description
          MessageReason:
            type: string
          UpdateMethod:
            type: object
            properties:
              Abbreviation:
                type: string
              Number:
                type: integer
              Title:
                type: string
            required:
            - Abbreviation
            - Number
            - Title
        required:
        - Errors
        - MessageReason
        - UpdateMethod
    required:
    - UpdatePatientDemographicsResult
  503ErrorRespSchema:
        type: object
        properties:
          executionContext:
            type: object
            properties:
              errors:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              warnings:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              infos:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
            required:
            - errors
            - warnings
            - infos
        required:
        - executionContext
    